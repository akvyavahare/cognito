//
// Copyright 2014-2018 Amazon.com,
// Inc. or its affiliates. All Rights Reserved.
//
// Licensed under the Amazon Software License (the "License").
// You may not use this file except in compliance with the
// License. A copy of the License is located at
//
//     http://aws.amazon.com/asl/
//
// or in the "license" file accompanying this file. This file is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
// CONDITIONS OF ANY KIND, express or implied. See the License
// for the specific language governing permissions and
// limitations under the License.
//

#import "Constants.h"

AWSRegionType const CognitoIdentityUserPoolRegion = AWSRegionAPSouth1;
NSString *const CognitoIdentityUserPoolId = @"ap-south-1_2y3QQduRa";
NSString *const CognitoIdentityUserPoolAppClientId = @"6pgn7oid305nj4bo2kkuje25ep";
NSString *const CognitoIdentityUserPoolAppClientSecret = @"1jhiap4genb0toaqbolevfnuih85pehfnmm9cftfde0d7i41tbgc";
